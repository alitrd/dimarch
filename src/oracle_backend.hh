// -*- mode: c++; c-basic-offset: 3; indent-tabs-mode: nil -*-

#ifndef DIMARCH_ORACLE_BACKEND_HH
#define DIMARCH_ORACLE_BACKEND_HH

//#include "service.hh"

#include "dimarch.hh"

#include <occi.h>

#include <string>
#include <map>
#include <iostream>

namespace dimarch {


   class oracle_backend : public dimarch {
      
      
   public:
      oracle_backend(std::string dbname, std::string user, std::string password);
      ~oracle_backend();
      
      virtual void archive(std::string name, float value, time_t create_time);
      virtual query_results* query(std::string q,
                                   timestamp begin_time,
                                   timestamp end_time);

   protected:
      
      int get_dbid(const char* name);
      int get_dbid(std::string name) { return get_dbid(name.c_str()); }
      
      
      // OCCI 
      oracle::occi::Environment *oraenv;
      oracle::occi::Connection  *oraconn; 
      oracle::occi::Statement   *orastmt_insert;
      oracle::occi::Statement   *orastmt_seldbids;
      oracle::occi::Statement   *orastmt_query;
     
      // mapping of service names to database IDs
      std::map < std::string , int > dbid_map;

   };


   // boost::posix_time::ptime oracle2boost_time(const oracle::occi::Timestamp t);
   // oracle::occi::Date       boost2oracle_time(const boost::posix_time::ptime t,
   //                                            oracle::occi::Environment* env);

   
};


#endif
