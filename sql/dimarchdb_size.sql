SELECT dbid, 
       (SELECT CAST(name AS varchar(30)) FROM dimarch_services WHERE dimarch_services.dbid=dimarch_data.dbid),
	count(1) AS cnt 
FROM dimarch_data
GROUP BY dbid
ORDER BY cnt;
