
#include "cfgfile.hh"
#include "logging.hh"
#include "config.h"

#include "Poco/Util/AbstractConfiguration.h"
#include "Poco/Util/XMLConfiguration.h"

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
#include <stdio.h>

using namespace dimarch;
using namespace std;

cfgfile* cfgfile::thecfgfile = 0;

cfgfile* cfgfile::instance(string cfgfilename)
{
  if (!thecfgfile) {

    if (cfgfilename=="") {
      cfgfilename =  (string)dimarchconfdir + "/dimarch.xml";
    }
    thecfgfile = new cfgfile(cfgfilename);
  }
  return thecfgfile;
}

cfgfile::cfgfile(string cfgfilename)
: cfg(NULL)
{

  try {
    cfg = new Poco::Util::XMLConfiguration(cfgfilename);
  }

  catch (Poco::FileNotFoundException ex) {
    cerr << "Configuration file \"" << cfgfilename << "\" not found" << endl;
    exit (-1);
  }

}

string cfgfile::get_backend()
{ return cfg->getString("database.backend", "influxdb");}

string cfgfile::get_dbname()
{ return cfg->getString("database.name");}

string cfgfile::get_dbtable()
{ return cfg->getString("database.table");}

string cfgfile::get_dbuser()
{ return cfg->getString("database.username", "");}

string cfgfile::get_dbpass()
{ return cfg->getString("database.password", "");}

string cfgfile::srvbase(int i)
{
  ostringstream basestream;
  basestream << "service[" << i << "]";
  pdebug.debug() << "srvbase: " << basestream.str() << endl;
  return basestream.str();
}


int cfgfile::get_n_services()
{
  //pdebug.information() << "nsrv" << endl;

  int nsrv = 0;

  try {
    char tmpstr[100];
    //for (int i=0; true; i++){
    while (1) {
      psyslog.information() << cfg->getString(srvbase(nsrv))<< endl;
      nsrv++;
    }
  }

  catch (const Poco::NotFoundException& notfound) {
    // no more
    psyslog.debug() << "no more services in config file" << endl;
  }

  return nsrv;
}

string cfgfile::get_service_pattern(int i)
{ return cfg->getString(srvbase(i)+"pattern"); }

float cfgfile::get_service_rel_deadband(int i)
{ return cfg->getDouble(srvbase(i)+"rel_deadband", 0.0); }

float cfgfile::get_service_abs_deadband(int i)
{ return cfg->getDouble(srvbase(i)+"abs_deadband", 0.0); }

float cfgfile::get_service_nolink(int i)
{ return cfg->getDouble(srvbase(i)+"nolink", nan("")); }

int cfgfile::get_service_timeout(int i)
{ return cfg->getInt(srvbase(i)+"timeout", 600); }
