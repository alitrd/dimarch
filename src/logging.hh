#ifndef DIMARCH_LOGGING_HH
#define DIMARCH_LOGGING_HH

#include <Poco/Logger.h>
#include <Poco/LogStream.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/NullChannel.h>
#include <Poco/SyslogChannel.h>

extern Poco::LogStream psyslog;
extern Poco::LogStream pdebug;

#endif
