CREATE OR REPLACE VIEW dimarch_stats AS
	SELECT	CAST( ( SELECT name 
			FROM dimarch_services 
			WHERE dimarch_services.dbid=dimarch_data.dbid) 
                      AS VARCHAR(40)) AS name, 
		COUNT(1) AS entries, 
		MIN(create_time) AS min_ctime, 
		MAX(create_time) AS max_ctime,
		MIN(insert_time) AS min_itime, 
		MAX(insert_time) AS max_itime
	FROM	dimarch_data
	GROUP BY dbid;


