#include <stdlib.h>
#include <string.h>
#include "influxdb.hpp"
#include "influxdb_backend.hh"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/algorithm/string/split.hpp"
#include "boost/algorithm/string/classification.hpp"
using namespace std;
namespace pt = boost::posix_time;

dimarch::influxdb_backend::influxdb_backend(std::string dbname, std::string measurementname, std::string user, std::string password){

 
influxdb_cpp::server_info influxCin("127.0.0.1", 8086, "parameters", user, password);
 std::string resp;
 influxdb_cpp::query(resp, "show databases", influxCin);
 influxdb_cpp::query(resp, "show measurements", influxCin);
 //cout << resp << endl;
}

void dimarch::influxdb_backend::archive(std::string name, float value, time_t create_time){

 influxdb_cpp::server_info influxCin("127.0.0.1", 8086, "parameters", "","");
  pt::ptime current_date_milliseconds = pt::microsec_clock::universal_time();

  pt::ptime time_t_epoch(boost::gregorian::date(1970, 1, 1));
  pt::time_duration diff = current_date_milliseconds - time_t_epoch;

  long utcepoch = diff.total_nanoseconds();

  vector<string> names;
  boost::split(names,name,boost::is_any_of(".")); 
  
  vector<string> channelinfo;
  boost::split(channelinfo, names[1],boost::is_any_of("_")); 
  
 
  int ret = influxdb_cpp::builder()
  .meas(channelinfo[0])
  .tag("sector", channelinfo[1])
  .tag("layer", channelinfo[2])
  .tag("type", names[2].substr(0,1))
  .tag("meas", names[3].substr(0,1))
  .field("value", value)
  .timestamp(utcepoch)
  .post_http(influxCin);

}

dimarch::query_results* dimarch::influxdb_backend::query(std::string querystr,
							 timestamp begin_time,
							 timestamp end_time){


 return NULL;
}

dimarch::influxdb_backend::~influxdb_backend(){
}
