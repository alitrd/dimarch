
#include "oracle_query_results.hh"
#include "time.h"

#include <boost/date_time/date.hpp>
#include <iostream>
#include <assert.h>

using namespace std;
namespace bptime = boost::posix_time;

dimarch::oracle_query_results::oracle_query_results()
    : orastmt(NULL), rset(NULL)
{}





dimarch::oracle_query_results::~oracle_query_results()
{
    if(rset)
        orastmt->closeResultSet(rset);
}

void dimarch::oracle_query_results::set_resultset(oracle::occi::ResultSet* r,
                                                  oracle::occi::Statement* s)
{
    rset = r;
    orastmt = s;
}


bool dimarch::oracle_query_results::next_entry()
{
    assert(rset != NULL);
    return rset->next();
}

std::string dimarch::oracle_query_results::get_name()
{
    assert(rset != NULL);
    return svcinfo[rset->getInt(1)].name;

    //return "not implemented";
}

int dimarch::oracle_query_results::get_dbid()
{
    assert(rset != NULL);
    return rset->getInt(1);
}

float dimarch::oracle_query_results::get_value()
{
    assert(rset != NULL);
    return rset->getFloat(2);
}

dimarch::timestamp dimarch::oracle_query_results::get_insert_time()
{ 
    assert(rset != NULL);

    oracle::occi::Timestamp ts = rset->getTimestamp(3);
    
    int year;
    unsigned int month, day, hour, min, sec, fs;

    // extract information from oracle timestamp
    ts.getDate           ( year,month, day );
    ts.getTime           ( hour, min, sec, fs );

    // oracle gives fractional seconds in nano-seconds, boost expects them in 
    // micro-seconds, so we need to convert
    fs /= 1000;
    
    // construct and return boost ptime object
    // return bptime::ptime(boost::gregorian::date(year,month,day),
    //                      bptime::time_duration(hour, min, sec, fs));

    return timestamp(year, month, day, hour, min, sec);

}

dimarch::timestamp dimarch::oracle_query_results::get_create_time()
{
    assert(rset != NULL);
    oracle::occi::Date ts = rset->getDate(4);
    
    int year;
    unsigned int month, day, hour, min, sec;

    // extract information from oracle timestamp
    ts.getDate ( year,month, day, hour, min, sec );

    // construct and return boost ptime object
    // return bptime::ptime(boost::gregorian::date(year,month,day),
    //                      bptime::time_duration(hour, min, sec));

    return timestamp(year, month, day, hour, min, sec);
}



// Local Variables:
//   mode: c++
//     c-basic-offset: 4
//     indent-tabs-mode: nil
// End:
