
#include "dimarch.hh"


time_t dimarch::query_results::get_insert_time_int()
{ 
  return get_insert_time().to_time_t(); 
}

time_t dimarch::query_results::get_create_time_int()
{ 
  return get_create_time().to_time_t(); 
}


std::string  dimarch::query_results::get_insert_time_string()
{ 
  return get_insert_time().to_simple_string(); 
}

std::string  dimarch::query_results::get_create_time_string()
{ 
  return get_create_time().to_simple_string(); 
}



void dimarch::query_results::add_datafield(std::string name, 
					   std::string alias, 
					   int dbid)
{
    svcinfo_t si;
    si.dbid  = dbid;
    si.name  = name;
    si.alias = alias;

    svcinfo[dbid] = si;
    dbids.push_back(dbid);
}




size_t dimarch::query_results::get_n_datafields() 
{
  return svcinfo.size();
}

int dimarch::query_results::get_datafield_dbid(size_t n)
{
  return dbids[n];
}

std::string dimarch::query_results::get_datafield_name(size_t n)
{
  return svcinfo[dbids[n]].name;
}

std::string dimarch::query_results::get_datafield_alias(size_t n)
{
  return svcinfo[dbids[n]].alias;
}




