
CREATE TABLE dimarch_services (
	dbid	NUMBER(6) PRIMARY KEY,
	name	VARCHAR2(255) UNIQUE NOT NULL
)
TABLESPACE users;

CREATE TABLE dimarch_data (
	dbid		NUMBER(6) NOT NULL REFERENCES dimarch_services,
	value		NUMBER NOT NULL,
	create_time	DATE NOT NULL,
	insert_time	TIMESTAMP NOT NULL,
	PRIMARY KEY (insert_time)
)
ORGANIZATION INDEX TABLESPACE users;

CREATE SEQUENCE dimarch_service_seq
START WITH 1
INCREMENT BY 1
NOMAXVALUE;

CREATE TRIGGER dimarch_service_trigger
BEFORE INSERT ON dimarch_services
FOR EACH ROW
BEGIN
	SELECT dimarch_service_seq.nextval INTO :new.dbid FROM dual;
END;
/
