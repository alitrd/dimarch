
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/date.hpp>

#include <iostream>
#include <sys/types.h>
#include <stdio.h>


#include "dimarch.hh"

using std::cerr;
using std::endl;

dimarch::timestamp::timestamp(int year, unsigned int month, unsigned int day,
			      unsigned int hour, unsigned int min, unsigned int sec)

{
    timeptr = new boost::posix_time::ptime( boost::gregorian::date(year, month, day),
                                            boost::posix_time::time_duration(hour, min, sec) );
}

dimarch::timestamp::timestamp (time_t x)
{
    timeptr = new boost::posix_time::ptime;
    *timeptr = boost::posix_time::from_time_t(x);
}

dimarch::timestamp::timestamp (std::string timestr)
{
    // This constructor uses the date(1) command via a pipe to convert 
    // a time string to time_t, i.e. the seconds since epoch. This number 
    // is then used to construct this timestamp.
    //
    // It would be nicer to have a direct call to a parser, but unfortunately
    // the coreutils package does not provide the parser in a separate library
    // and copying it over from there and maintaining a local copy seems too
    // complicated.


    time_t timeint;


    // create the pipe to talk to `date`
    int pipefd[2];
   
    if (pipe(pipefd)) {
        perror("Error opening pipe");
    }
    
    
    if (fork() == 0) {
        // This is the child - will execute `date`
     
        // Close the reading end of the pipe
        close(pipefd[0]);
     
        // Connect stdout to the pipe
        dup2(pipefd[1],1);
     
        // execute `date` to convert string to time since epoch
        execlp("date", "date", "-d", timestr.c_str(), "+%s", NULL);
     
        // ERROR: execlp does not return if successful
        // maybe there should be nicer error handling...
        cerr << "ERROR: cannot execute 'date'" << endl;
        exit(-1);
     
    } else {

        // Close writing end of pipe
        close(pipefd[1]);
     
        FILE *pipef = fdopen(pipefd[0], "r");
     
        // Scan into temporary var to avoid warning in fscanf about format 
        // mismatch %u <-> time_t
        unsigned int timetmp;
        if ( 1 != fscanf(pipef, "%u", &timetmp) ) {
            cerr << "ERROR: cannot parse output of 'date'" << endl;
            exit(-1);
        }
        timeint = timetmp;

    }

    // construct the timeptr
    timeptr = new boost::posix_time::ptime;
    *timeptr = boost::posix_time::from_time_t(timeint);

}

dimarch::timestamp::~timestamp()
{
  delete timeptr;
}


int           dimarch::timestamp::year()   { return timeptr->date().year(); }
unsigned int  dimarch::timestamp::month()  { return timeptr->date().month(); }
unsigned int  dimarch::timestamp::day()    { return timeptr->date().day(); }
unsigned int  dimarch::timestamp::hour()   { return timeptr->time_of_day().hours(); }
unsigned int  dimarch::timestamp::minute() { return timeptr->time_of_day().minutes(); }
unsigned int  dimarch::timestamp::second() { return timeptr->time_of_day().seconds(); }


time_t dimarch::timestamp::to_time_t()
{
  struct tm td_tm = to_tm(*timeptr);
  return mktime(&td_tm);
}

std::string dimarch::timestamp::to_simple_string() 
{ 
  std::string s = boost::posix_time::to_simple_string(*timeptr); 
  return s;
}

std::string dimarch::timestamp::to_iso_string()
{ 
  std::string s = boost::posix_time::to_iso_string(*timeptr); 
  return s;
}



std::ostream& dimarch::timestamp::write_to_stream(std::ostream& os)
{
  return os << (*timeptr);
}

std::ostream& operator<<(std::ostream& os, dimarch::timestamp ts)
{ 
    return ts.write_to_stream(os); 
}




// Local Variables:
//   mode: c++
//     c-basic-offset: 4
//     indent-tabs-mode: nil
// End:
