#ifndef INFLUXDB_BACKEND_HH
#define INFLUXDB_BACKEND_HH

#include "dimarch.hh"
#include <string>
#include <map>
#include <iostream>

namespace dimarch {
	class influxdb_backend : public dimarch {

	public:
	  influxdb_backend(std::string dbname, std::string measurename, std::string user, std::string password);
	  ~influxdb_backend();
          virtual void archive(std::string name, float value, time_t create_time);
          virtual query_results* query(std::string q,
                                   timestamp begin_time,
                                   timestamp end_time);

	 
//	protected:


	};
};

#endif 
