#ifndef DIMARCH_CFGFILE_HH
#define DIMARCH_CFGFILE_HH

#include "dimarch.hh"

#include <string>

namespace Poco { namespace Util { class AbstractConfiguration; }}

namespace dimarch {

  class cfgfile {
  public:
    static cfgfile* instance(std::string cfgfilename="");

    std::string get_backend();
    std::string get_dbname();
    std::string get_dbtable();
    std::string get_dbuser();
    std::string get_dbpass();

    int          get_n_services();
    std::string  get_service_pattern(int i);
    float        get_service_rel_deadband(int i);
    float        get_service_abs_deadband(int i);
    float        get_service_nolink(int i);
    int          get_service_timeout(int i);


  protected:
    Poco::Util::AbstractConfiguration *cfg;
    std::string srvbase(int i);

  private:
    cfgfile(std::string cfgfilename);
    static cfgfile* thecfgfile;

  };
}

#endif
