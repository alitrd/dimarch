#include "dimarch.hh"

#include <iostream>

#include "cfgfile.hh"
#include "logging.hh"

#include "oracle_backend.hh"
#include "influxdb_backend.hh"
using namespace std;

dimarch::dimarch* create_dimarch()
{
  dimarch::cfgfile* cfg = dimarch::cfgfile::instance();

  if (cfg->get_backend() == "oracle") {

    return new dimarch::oracle_backend(cfg->get_dbname(),
                                       cfg->get_dbuser(),
                                       cfg->get_dbpass());

  } else if (cfg->get_backend() == "influxdb") {

    return new dimarch::influxdb_backend(cfg->get_dbname(),
                                         cfg->get_dbtable(),
                                         cfg->get_dbuser(),
                                         cfg->get_dbpass());

  } else {
    psyslog.error() << "unknown database backend \""
                    << cfg->get_backend() << "\"" << endl;
  }
}


void destroy_dimarch(dimarch::dimarch* da)
{
  delete da;
}


dimarch::query_results* dimarch::dimarch::query(std::string q, time_t begin_time, time_t end_time)
{
  return query(q, timestamp(begin_time), timestamp(end_time));

}

dimarch::query_results* dimarch::dimarch::query(std::string q,
						std::string begin_time,
						std::string end_time)
{
  return query(q, timestamp(begin_time), timestamp(end_time));

}
