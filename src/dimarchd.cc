
#include "dimarch_daemon.hh"

#include "dic.hxx"
#include "dis.hxx"

#include <iostream>
#include <fstream>
#include <sstream>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"
#include "cfgfile.hh"
#include "logging.hh"

using namespace std;

int daemonize()
{
    /* +creating a child process.
        +close file descriptor
        (+set signal handler) */

    switch(fork()) {
        case -1:
            perror("fork");
            return errno;
        case 0:
            setsid();
            break;
        default:
            _exit(0);
    }

    //pid file
    ofstream fout("/var/run/dimarchd/dimarchd.pid",
                  std::ios::out|std::ios::trunc);
    fout << getpid() << endl;
    fout.close();

    if ( chdir("/") ) {
      perror("chdir");
      exit(-1);
    }

//     int fd = 0;
//     int fdlimit = sysconf(_SC_OPEN_MAX);
//     while (fd<fdlimit) {
//       //close(fd++);
//     }

//     open("/dev/null", O_RDWR);
//     dup2(0,1);
//     dup2(0,2);

    //signal(SIGALRM, SIG_IGN);
    //signal(SIGCHLD, SIG_IGN);
    //signal(SIGHUP , SIG_IGN);
    //signal(SIGTERM, SIG_DFL);
    //signal(SIGPIPE, SIG_DFL);

    umask(0);

    return 0;
}

int main(int argc, char** argv)
{


   string cfgfilename = (string)dimarchconfdir + "/dimarch.xml";
   bool run_daemonized = true;

   int c;

   while (1) {
     //int this_option_optind = optind ? optind : 1;
     int option_index = 0;
     static struct option long_options[] = {
       {"configfile",  1, 0, 'c'},
       {"debug",       1, 0, 'D'},
       {0, 0, 0, 0}
     };

     c = getopt_long (argc, argv, "c:D",
		      long_options, &option_index);
     if (c == -1)
       break;

     switch (c) {
     case 'c':
       cfgfilename = optarg;
       break;

     case 'D':
       run_daemonized = false;
       break;

     default:
       cerr << "?? getopt returned unknown character ??" << endl;
     }
   }

   if (optind < argc) {
     cerr << "usage: dimarchd <options> ..." << endl;
     exit(-1);
   }

   // -----------------------------------------------------------------------
   // read config file
   dimarch::cfgfile* cfg = dimarch::cfgfile::instance(cfgfilename);

   // -----------------------------------------------------------------------
   // set up logging and fork to background
   if (run_daemonized) {

     Poco::Channel* syslogch = new Poco::SyslogChannel;
     syslogch->setProperty( "name",     "dimarchd");
     syslogch->setProperty( "facility", "SYSLOG_DAEMON");
     syslogch->setProperty( "options",  "0");

     Poco::Logger::root().setChannel(new Poco::ConsoleChannel);
     Poco::Logger::get( "syslog" ).setChannel(syslogch);
     Poco::Logger::get( "debug"  ).setChannel(new Poco::NullChannel);

     daemonize();

   } else {

     Poco::Channel* ch = new Poco::ConsoleChannel;

     Poco::Logger::root().setChannel(ch);
     Poco::Logger::get( "syslog" ).setChannel(ch);
     Poco::Logger::get( "debug"  ).setChannel(ch);

   }

   psyslog.information() << "starting dimarchd" << endl;

   dimarch_daemon daemon;

   

   int nsrv = cfg->get_n_services();
   psyslog.information() << "Subscribing to " << nsrv << " service groups" << endl;

   for (int i=0; i<nsrv;i++) {

     daemon.subscribe( cfg->get_service_pattern(i),
                       cfg->get_service_nolink(i),
                       cfg->get_service_abs_deadband(i),
                       cfg->get_service_rel_deadband(i),
                       cfg->get_service_timeout(i) );

   }

   daemon.show_subscriptions();
   daemon.update_connections();

   char defsrvlist[1];
   defsrvlist[0] = 0;
   int oldsize = 0;


   list <int> dimupd;
   float dimrate = 0.;
   DimService dimratesrv("DIMARCH_RATE_DIM",dimrate);

   list <int> dbupd;
   float dbrate = 0.;
   DimService dbratesrv("DIMARCH_RATE_DB",dbrate);

   DimServer::start("DIMARCH");

   //cout << "entering main loop" << endl;


   while(1) {
      DimInfo srvlist("DIS_DNS/SERVER_LIST",defsrvlist);
      //std::cout << "Size: " << srvlist.getSize() << std::endl;

      while(srvlist.getSize() == 0) sleep(1);

      if (oldsize != srvlist.getSize()) {

        daemon.update_connections();
        //std::cout << srvlist.getString() << std::endl;
        oldsize = srvlist.getSize();
      }


      // update rate of DIM service changes
      dimupd.push_back(daemon.get_n_dim_updates());

      if(dimupd.size() >= 2) {
        dimrate = float(dimupd.back() - dimupd.front())
        / float(dimupd.size() - 1);
      }

      if(dimupd.size() >= 60) {
        dimupd.pop_front();
      }

      dimratesrv.updateService();

      // update rate of writes to DB
      dbupd.push_back(daemon.get_n_db_updates());

      if(dbupd.size() >= 2) {
        dbrate = float(dbupd.back() - dbupd.front())
        / float(dbupd.size() - 1);
      }

      if(dbupd.size() >= 60) {
        dbupd.pop_front();
      }

      dbratesrv.updateService();

      //cout << dimrate << "  " << dbrate << endl;

      sleep(1);
   }


}
