// -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil -*-

// =================================================================
//
//  This should be an interface for external programs to access the
//  data stored in a DimArch database. The idea is to use this
//  interface from a relatively simple ROOT macro separate the OCCI
//  interface from ROOT, so that a single libdimarch.so can be used
//  by different versions of ROOT.
//
// =================================================================



#ifndef DIMARCH_HH
#define DIMARCH_HH

namespace boost {
    namespace posix_time {
        class ptime;
    };
};

#include <string>
#include <map>
#include <vector>

namespace dimarch {

    class timestamp {

    public:
        timestamp (int year, unsigned int month, unsigned int day,
                   unsigned int hour=0, unsigned int minute=0, unsigned int second=0);

        timestamp (time_t x);
        timestamp (std::string x);

        ~timestamp();

        int           year();
        unsigned int  month();
        unsigned int  day();
        unsigned int  hour();
        unsigned int  minute();
        unsigned int  second();

        std::ostream& write_to_stream(std::ostream& os);

        time_t       to_time_t();
        std::string  to_simple_string();
        std::string  to_iso_string();

    protected:
        boost::posix_time::ptime *timeptr;

    };



    // Class to hold the results from a query.
    class query_results {

    public:
        virtual ~query_results() {};


        // -----------------------------------------------------------------
        // query of data values
        virtual bool next_entry() = 0;

        virtual std::string  get_name() = 0;
        virtual int          get_dbid() = 0;
        virtual float        get_value() = 0;
        virtual timestamp    get_insert_time() = 0;
        virtual timestamp    get_create_time() = 0;

        virtual time_t       get_insert_time_int();
        virtual time_t       get_create_time_int();

        virtual std::string  get_insert_time_string();
        virtual std::string  get_create_time_string();

        // -----------------------------------------------------------------
        // information about data fields in this query

        virtual size_t       get_n_datafields();
        virtual int          get_datafield_dbid(size_t n);
        virtual std::string  get_datafield_name(size_t n);
        virtual std::string  get_datafield_alias(size_t n);

        // add information about datafields
        virtual void add_datafield(std::string name, std::string alias, int dbid);



    protected:
        struct svcinfo_t {
            int          dbid;
            std::string  name;
            std::string  alias;
        };

        std::map<int,svcinfo_t> svcinfo;

        // vector to keep information about available dbid's
        std::vector<int> dbids;

    };


    // General interface class to a dimarch database
    class dimarch {

    public:
        virtual ~dimarch() {};

        virtual query_results* query(std::string q,
                                     timestamp begin_time,
                                     timestamp end_time) = 0;

        // query functions with other types to specify the time range
        virtual query_results* query(std::string q, time_t begin_time, time_t end_time);
        virtual query_results* query(std::string q, std::string begin, std::string end);

        // query_results* query(std::string q, time_t begin_time, time_t end_time)
        // {
        //     return query(q,
        //                  boost::posix_time::from_time_t(begin_time),
        //                  boost::posix_time::from_time_t(end_time));
        // }

        virtual void archive(std::string name, float value, time_t create_time) = 0;


    };


    // class exception : public std::exception {

    // public:
    //     exception(std::string what) : payload(what) {}
    //     ~exception() throw() {}

    //     virtual const char* what() const throw()
    //     {
    //         return payload.c_str();
    //     }

    // protected:
    //     std::string payload;

    // };

}

std::ostream& operator<<(std::ostream& os, dimarch::timestamp ts);

extern "C" {
    // external interface to create and destroy an instance of dimarch
    //
    // This facilitates the interfacing to this library using dlopen and
    // friends: only two C-type functions are necessary to construct and
    // destruct the object, effectively replacing the new and delete
    // operators, which would need access to full type information.

    dimarch::dimarch* create_dimarch();
    void destroy_dimarch(dimarch::dimarch* da);
}


#endif
