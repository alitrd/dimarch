// -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil -*-

#ifndef DIMARCH_ORACLE_QUERY_RESULTS_HH
#define DIMARCH_ORACLE_QUERY_RESULTS_HH

#include "dimarch.hh"
#include "oracle_backend.hh"

#include <map>
#include <occi.h>

namespace dimarch {

    class oracle_query_results : public query_results {

    public:

        oracle_query_results();
        virtual ~oracle_query_results();

        // Tell this class about the OCCI ResultSet to retrieve the data.
        // The Statement is needed to close the ResultSet in the destructor.
        void set_resultset(oracle::occi::ResultSet* r,
                           oracle::occi::Statement* s);

        
        virtual bool next_entry();

        virtual std::string  get_name();
        virtual int          get_dbid();
        virtual float        get_value();
        virtual timestamp    get_insert_time();
        virtual timestamp    get_create_time();

    protected:
        oracle_backend           *backend; // pointer to controlling DB backend
        oracle::occi::Statement  *orastmt; // used to close result set
        oracle::occi::ResultSet  *rset;    // class to access query results

        // struct svcinfo_t {
        //     int          dbid;
        //     std::string  name;
        //     std::string  alias;
        //     float        value;
        // };

        // std::map<int,svcinfo_t> svcinfo;

    };

};

#endif
