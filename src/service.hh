#ifndef DIMARCH_SERVICE_HH
#define DIMARCH_SERVICE_HH

#include <string>
#include <iostream>

#include <dic.hxx>
#include <math.h>

namespace dimarch {

   class service
   {
   public:

      service ( DimStampedInfo *  _dimsrv_, 
		float             _nolink_, 
		float             _abs_deadband_, 
		float             _rel_deadband_, 
		int               _timeout_)

	 : m_dimsrv(_dimsrv_),
	   m_abs_deadband(_abs_deadband_), 
	   m_rel_deadband(_rel_deadband_), 
	   m_lastval(_nolink_),
	   m_lasttime(0),
	   m_timeout(_timeout_),
	   m_nolink(_nolink_)
      {}
      
      // Access to DimInfo members as C++/STL strings
      inline std::string  name()    {return m_dimsrv->getName();}
      inline std::string  format()  {return m_dimsrv->getFormat();}


      // Getters for data members
      inline float abs_deadband() const {return m_abs_deadband;}
      inline float rel_deadband() const {return m_rel_deadband;}
      inline float lastval()  const {return m_lastval;}
      inline float nolink()   const {return m_nolink;}
      
      
      // Access to DIM service data
      inline float value() 
      {
	 if(format() == "F" || format() == "F:1" )
	    return m_dimsrv->getFloat();
	 else if(format() == "L" || format() == "I")
	    return m_dimsrv->getInt();
	 else
	    return 0.;
      }

      inline void update() 
      { 
	 m_lastval = value(); 
	 m_lasttime = time(NULL);
      }

      inline bool due_for_update() 
      {
	 return ( outside_deadband() || 
		  abs(time(NULL) - m_lasttime) > m_timeout);
      }

      inline bool outside_deadband() 
      { 

	 // the value is outside the deadband if it outside the
	 // relative AND the absolute deadband

	 return ( ( fabs(value()-m_lastval) > m_abs_deadband ) &&
		  ( fabs((value()-m_lastval)/m_lastval) > m_rel_deadband )); 
      }

      inline bool inside_deadband() 
      { return !outside_deadband(); }

      inline time_t timestamp()
      { return m_dimsrv->getTimestamp(); }


   protected:

      DimStampedInfo *m_dimsrv;
      
      float m_abs_deadband;
      float m_rel_deadband;
      float m_lastval;
      int   m_lasttime;
      int   m_timeout;
      float m_nolink;
      

      static const float lastval_init = -9876543.21;


	   //bool operator< (service_info& other) { return name() < other.name(); }
      
//       service_info& operator= (service_info& other) { 
	 
// 	 m_name     = other.name();
// 	 m_format   = other.format();
// 	 m_type     = other.type();
// 	 m_dbid     = other.dbid();
// 	 m_deadband = other.deadband();
// 	 m_lastval  = other.lastval();

// 	 return *this;
//       }
      

   }; // class service


   
}; // namespace dimarch

//std::ostream& operator<<(std::ostream& s, service_info& x);

#endif
