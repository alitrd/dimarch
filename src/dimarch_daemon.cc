#include "dimarch_daemon.hh"

#include "logging.hh"
#include <iostream>


using namespace std;

dimarch_daemon::dimarch_daemon()
   : n_dim_updates(0),
     n_db_updates(0)
{
  dimarchdb = create_dimarch();
}


dimarch_daemon::~dimarch_daemon()
{
  destroy_dimarch(dimarchdb);
}


void dimarch_daemon::update_connections()
{
   DimBrowser dbr;

   char *name;
   char *format;
   int type;

   typedef map<std::string, DimInfo*> conn_map_t;
   conn_map_t connections;

   for(service_map_t::iterator itr = service_map.begin();
       itr != service_map.end(); itr++) {

      connections[itr->second->name()] = itr->first;

   }

   for (list<subscription_t>::iterator subscr = subscriptions.begin();
	subscr != subscriptions.end(); subscr++) {

      dbr.getServices(subscr->pattern.c_str());

      while( (type = dbr.getNextService(name, format)) ) {

	 if(connections.find(name) == connections.end()) {

	    add_service(name, format,
			subscr->nolink, subscr->abs_deadband,
			subscr->rel_deadband, subscr->timeout);
	 } else {
	    // mark the connection as good
	    connections[name] = 0;
	 }
      }
   }

   for(conn_map_t::iterator itr = connections.begin();
       itr != connections.end(); itr++) {

      if(itr->second != 0) {
	 delete service_map[itr->second];
	 delete itr->second;
	 service_map.erase(service_map.find(itr->second));
      }
   }

}


void dimarch_daemon::add_service(std::string name,
				 std::string format,
				 float nolink,
				 float abs_deadband,
				 float rel_deadband,
				 int timeout)
{

   DimStampedInfo *dimsrv = NULL;
   //dimarch::service *srv = NULL;

   if ( format=="F" || format=="F:1" ) {
      dimsrv = new DimStampedInfo(name.c_str(), 600, nolink, this);

      psyslog.information() << "subscribing to float service \""
			    << name << "\"" << endl;

   } else if ( format=="L" || format=="I" ) {
      dimsrv = new DimStampedInfo(name.c_str(), 600, int(nolink), this);

      psyslog.information() << "subscribing to int service \""
			    << name << "\"" << endl;

   } else {
     psyslog.error() << "Service " << name << " has unknown format "
		     << format << ". Ignoring." << endl;
      return;
   }

   service_map[dimsrv] = new dimarch::service(dimsrv, nolink,
					      abs_deadband, rel_deadband,
					      timeout);


}



void dimarch_daemon::subscribe(std::string name_pattern,
			       float nolink, float abs_deadband,
			       float rel_deadband, int timeout)
{
   subscription_t subsc;
   subsc.pattern = name_pattern;
   subsc.nolink = nolink;
   subsc.rel_deadband = rel_deadband;
   subsc.abs_deadband = abs_deadband;
   subsc.timeout = timeout;

   subscriptions.push_back(subsc);
}


void dimarch_daemon::show_subscriptions()
{

   for (list<subscription_t>::iterator itr=subscriptions.begin();
	itr!=subscriptions.end(); itr++) {


      psyslog.notice() << "subscribed to pattern: "
		       << itr->pattern << endl;

   }

}

void dimarch_daemon::infoHandler()
{

  dimarch::service *srv = service_map[getInfo()];

  n_dim_updates++;

  if( ! srv->due_for_update() ) {
    return;
  }

  dimarchdb->archive(srv->name(), srv->value(), srv->timestamp());
  n_db_updates++;

  usleep(10000); // 10 ms
  srv->update();

}
