// ======================================================================
//
//  dimarch-retrieve: very simple program to demonstrate the use of the
//                    query functionality of dimarch.
//
//  This program is not really useful right now, but mainly used for
//  testing.
//
// (c) 2012 - Tom Dietel <tom@dietel.net>
//
// ======================================================================

#include "dimarch_daemon.hh"

#include <iostream>
#include <iomanip>
#include <dimarch.hh>

using namespace std;

int main()
{
    dimarch::dimarch* da = create_dimarch();

    dimarch::query_results* res =
        da->query("iseg2dim.bus0.mod30.ch02.VMeas", "30 min ago", "now");
    //da->query("HV.00_4_4.DRIFT.VMeas", "30 min ago", "now");

    for (size_t i=0; i<res->get_n_datafields(); i++) {
        cout << i << ": " << res->get_datafield_dbid(i) << endl;
    }

    while ( res->next_entry() ) {

        cout << res->get_name() << "[" << res->get_dbid() << "]"
             << " @ " << res->get_insert_time() << " - "
             << res->get_create_time() << ": "
             << res->get_value() << endl;
    }



}



// Local Variables:
//   mode: c++
//     c-basic-offset: 4
//     indent-tabs-mode: nil
// End:
