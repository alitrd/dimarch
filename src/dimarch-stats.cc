#include "dimarch_daemon.hh"

#include <iostream>
#include <iomanip>
#include <occi.h>

using namespace std;
using namespace oracle::occi;

int main()
{

   Environment *oraenv  = Environment::createEnvironment();
   Connection  *oraconn = oraenv->createConnection("alice", "alice123", 
						   "//trd04/dimarch");

   Statement   *stmt = oraconn->createStatement();


   // get timestamps for now, 1 hour/day/week ago
   
   stmt->setSQL("SELECT current_timestamp FROM dual");
   ResultSet *rs = stmt->executeQuery();
   rs->next();
   
   Timestamp now = rs->getTimestamp(1);

   stmt->closeResultSet(rs);

   
   Timestamp hour  = now.intervalSub(IntervalDS(oraenv,0,1,0,0));
   Timestamp day   = now.intervalSub(IntervalDS(oraenv,1,0,0,0));
   Timestamp week  = now.intervalSub(IntervalDS(oraenv,7,0,0,0));
   Timestamp month = now.intervalSub(IntervalYM(oraenv,0,1));


   cout << "now   = " 
	<< now.toText("dd/mm/yyyy hh24:mi:ss [tzh:tzm]",0)
	<< endl;

   cout << "hour  = " 
	<< hour.toText("dd/mm/yyyy hh24:mi:ss [tzh:tzm]",0)
	<< endl;

   cout << "day  = " 
	<< day.toText("dd/mm/yyyy hh24:mi:ss [tzh:tzm]",0)
	<< endl;

   cout << "week  = " 
	<< week.toText("dd/mm/yyyy hh24:mi:ss [tzh:tzm]",0)
	<< endl;

   cout << "month  = " 
	<< month.toText("dd/mm/yyyy hh24:mi:ss [tzh:tzm]",0)
	<< endl;



   // query statistics
   
   stmt->setSQL((string)
		"SELECT dbid,"+
		"    ( SELECT name "+
		"      FROM     dimarch_services "+
		"      WHERE dimarch_services.dbid = dimarch_data.dbid"+
		"    ) AS name, "+
		"    COUNT(1), "+
  		"    SUM(1), "+
		//"    SUM(CASE WHEN insert_time>:2 THEN 1 ELSE 0 END), "+
//  		"    SUM(CASE WHEN insert_time>:3 THEN 1 ELSE 0 END), "+
//  		"    SUM(CASE WHEN insert_time>:4 THEN 1 ELSE 0 END), "+
//  		"    SUM(CASE WHEN insert_time>:5 THEN 1 ELSE 0 END), "+
		"    MAX(insert_time), MIN(insert_time)"+
		"FROM dimarch_data "+
		"WHERE insert_time < :1 "+
		//"  AND insert_time > :2 "+
		//		"  AND insert_time > :4 "+
		" AND insert_time > to_date('2011-01-01','yyyy-mm-dd') "+
		"GROUP BY dbid");
   
    stmt->setTimestamp(1,now);
//    stmt->setTimestamp(2,hour);
//    stmt->setTimestamp(3,day);
//    stmt->setTimestamp(2,month);
//    stmt->setTimestamp(5,month);

				     
// 		"SELECT name,dbid,value," + 
// 		"TO_CHAR(create_time,'YYYY-MM-DD HH24:MI:SS')," +
// 		"TO_CHAR(insert_time,'YYYY-MM-DD HH24:MI:SS') " +
// 		"FROM dimarch_services NATURAL JOIN dimarch_data " +
// 		"ORDER BY insert_time");

   //cout << stmt->getSQL() << endl;
   
				     

   rs = stmt->executeQuery();

   while(rs->next()) {
     cout << setw(30)  << left  << rs->getString(2) << "  "
	  << setw(5)   << right << rs->getInt(1) << "  "
 	  << setw(7)   << right << rs->getInt(3) << "  "
 	  << setw(7)   << right << rs->getInt(4) << "  "
// 	  << setw(8)   << right << rs->getInt(7) << "  "
	  << rs->getTimestamp(5).toText("yyyy-mm-dd hh24:mi",0) << "   "
	  << rs->getTimestamp(6).toText("yyyy-mm-dd hh24:mi",0)
	  << endl;

   }
   



   oraenv->terminateConnection(oraconn);
   Environment::terminateEnvironment(oraenv);
}
