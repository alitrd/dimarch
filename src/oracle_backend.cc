// -*- mode: c++; c-basic-offset: 3; indent-tabs-mode: nil -*-

#include "oracle_backend.hh"
#include "oracle_query_results.hh"
#include "logging.hh"

#include <boost/date_time/local_time/local_time.hpp>

using namespace std;
using namespace oracle::occi;

// dimarch::oracle_backend::oracle_backend()
// {
//    // would be best to consult a config file about the DB connection...


//    try {

//       oraenv  = Environment::createEnvironment(Environment::THREADED_MUTEXED);
//       oraconn = oraenv->createConnection("alice","alice123","//trd03/xe");

//       // Insert the data into the database
//       orastmt = oraconn->createStatement();
      
//       orastmt->setSQL
// 	 ((string)
// 	  "INSERT INTO dimarch_data (dbid,value,create_time,insert_time) " + 
// 	  "VALUES (:1,:2," + 
// 	  "        :3," +
// 	  "        CURRENT_TIMESTAMP AT TIME ZONE 'UTC')");


//    }

//    catch (SQLException &sqlExcp) {
//       cout << "Caught an exception: " << sqlExcp.what() << endl;
      
//       exit (-1);
//       //cerr <<sqlExcp.getErrorCode << ": " << endl;
//    }


// }






dimarch::oracle_backend::oracle_backend(string dbname, string user, string password)
{
   try {

      oraenv  = Environment::createEnvironment(Environment::THREADED_MUTEXED);
      oraconn = oraenv->createConnection(user.c_str(),password.c_str(),dbname.c_str());


      // create SQL statement to archive a value in the DB
      orastmt_insert = oraconn->createStatement();
      orastmt_insert->setSQL
	 ( (string)
           "INSERT INTO dimarch_data (dbid,value,create_time,insert_time) " + 
           "VALUES (:1,:2," + 
           "        :3," +
           "        CURRENT_TIMESTAMP AT TIME ZONE 'UTC')");


      // create SQL statement to populate the temporary table 
      // holding the information which data points to retrieve 
      // in the next query
      orastmt_seldbids = oraconn->createStatement();
      orastmt_seldbids->setSQL
         ( (string)
           "INSERT INTO seldbids VALUES (:1)");
      //"SELECT dbid " + 
      //"FROM dimarch_services " +
      //"WHERE name=:1");

      // create SQL statement to execute a query
      orastmt_query = oraconn->createStatement();
      orastmt_query->setSQL
         ( (string)
           "SELECT dbid,value," + 
           "       insert_time,create_time " +
           "FROM dimarch_data " +
           "WHERE dbid IN (SELECT dbid FROM seldbids) " +
           "  AND insert_time > :1 " + 
           "  AND insert_time < :2 "+
           "ORDER BY insert_time");
   }

   catch (SQLException &sqlExcp) {
      cout << "Caught an exception: " << sqlExcp.what() << endl;
      
      exit (-1);
      //cerr <<sqlExcp.getErrorCode << ": " << endl;
   }

}



dimarch::oracle_backend::~oracle_backend()
{

   oraconn->terminateStatement(orastmt_insert);
   oraconn->terminateStatement(orastmt_seldbids);
   oraconn->terminateStatement(orastmt_query);
   oraenv->terminateConnection(oraconn);
   Environment::terminateEnvironment(oraenv);

}


void dimarch::oracle_backend::archive(std::string name, float value, time_t create_time)
{
   try {
      
      struct tm* srv_tm   = gmtime(&create_time);
      int dbid = get_dbid(name.c_str());

      Date oradate(oraenv,
		   srv_tm->tm_year + 1900,
		   srv_tm->tm_mon + 1,
		   srv_tm->tm_mday,
		   srv_tm->tm_hour,
		   srv_tm->tm_min,
		   srv_tm->tm_sec );

      pdebug.information() << "dimarch::oracle_backend::archive: write " 
                           << name << " {" << dbid << "} = " << value << endl;

      orastmt_insert->setInt   (1, dbid);
      orastmt_insert->setFloat (2, value);
      orastmt_insert->setDate  (3, oradate);
      orastmt_insert->executeUpdate();
      oraconn->commit();

   }      

   catch (SQLException &sqlExcp) {
      cout << "Caught an exception: " << sqlExcp.what() << endl;
      
      return;
   }
}

dimarch::query_results* dimarch::oracle_backend::query(std::string querystr,
                                                       timestamp begin_time,
                                                       timestamp end_time)
{

   oracle_query_results* result = new oracle_query_results;

   // create a string that will be split to extract the desired fields
   std::string spec = querystr;

   try {
      int idx;
      do {
         idx = spec.find(',');
         
         string name = spec.substr(0,idx);
         string alias;
         
         size_t sep = name.find('>');
         if ( sep != string::npos) {
            alias = name.substr(sep+1);
            name = name.substr(0,sep);
         } else {
            alias = name;
         }
         
         int dbid = get_dbid(name);
         result->add_datafield(name, alias, dbid);
         
         orastmt_seldbids->setInt(1,dbid);
         orastmt_seldbids->execute();
         
         spec = spec.substr(idx+1);
         
      } while (idx>=0);
   }
   
   catch (SQLException &sqlExcp) {
      cout << "Caught an exception in preparing seldbids: " << sqlExcp.what() << endl;
      return NULL;
   }
      
   oracle::occi::ResultSet* rset;

   try {

       orastmt_query->setDate(1, oracle::occi::Date( oraenv, 
                                                     begin_time.year(), 
                                                     begin_time.month(), 
                                                     begin_time.day(), 
                                                     begin_time.hour(), 
                                                     begin_time.minute(), 
                                                     begin_time.second()));

       orastmt_query->setDate(2, oracle::occi::Date( oraenv, 
                                                     end_time.year(), 
                                                     end_time.month(), 
                                                     end_time.day(), 
                                                     end_time.hour(), 
                                                     end_time.minute(), 
                                                     end_time.second()));
                              
       
       //orastmt_query->setDate(2, boost2oracle_time(end_time, oraenv));

       //orastmt_query->setTimestamp(1, oracle::occi::Timestamp(oraenv,2000, 1, 1));
       //orastmt_query->setTimestamp(2, oracle::occi::Timestamp(oraenv,2012,11,6,9,0,0));
       rset = orastmt_query->executeQuery();
   }
   
   catch (SQLException &sqlExcp) {
      cout << "Caught an exception in query: " << sqlExcp.what() << endl;
      return NULL;
   }

   result->set_resultset(rset, orastmt_query);
   return result;

}


// void dimarch::db::init()
// {
//    try {
      


//       // check for existing structures in dimarch DB
//       // ===========================================

//       struct {
//          bool service_seq;
//          bool services;
//          bool data;
//          bool stats;
//       } exists;

//       exists.services = false;
//       exists.data = false;
//       exists.stats = false;


//       Statement *stmt = oraconn->createStatement();

//       // check if dimarch tables exist in DB
      
//       stmt->setSQL("SELECT sequence_name FROM user_sequences WHERE sequence_name LIKE 'dimarch_%");
      
//       ResultSet *rs = stmt->executeQuery();
      
//       while(rs->next()) {
//          if (rs->getString(1) == "dimarch_services" ) exists.services = true;;
//          if (rs->getString(1) == "dimarch_data"     ) exists.data     = true;;
//       }

//       stmt->closeResultSet (rs);


//       // check if dimarch tables exist in DB
      
//       stmt->setSQL("SELECT table_name FROM user_tables WHERE table_name LIKE 'dimarch_%");
      
//       ResultSet *rs = stmt->executeQuery();
      
//       while(rs->next()) {
//          if (rs->getString(1) == "dimarch_services" ) exists.services = true;;
//          if (rs->getString(1) == "dimarch_data"     ) exists.data     = true;;
//       }

//       stmt->closeResultSet (rs);


//       // check if dimarch views exist in DB
      
//       stmt->setSQL("SELECT view_name FROM user_view WHERE view_name LIKE 'dimarch_%");
      
//       ResultSet *rs = stmt->executeQuery();
      
//       while(rs->next()) {
//          if (rs->getString(1) == "dimarch_stats") exists.stats = true;;
//       }

//       stmt->closeResultSet (rs);


//       // create non-existent structures
//       // ==============================


//       if ( ! exists.service_seq ) {
//          stmt->setSQL((string)
//                       "CREATE TABLE
         

//       }




//       oraconn->terminateStatement(stmt);
//    }      

//    catch (SQLException &sqlExcp) {
//       cout << "Caught an exception: " << sqlExcp.what() << endl;
      
//       return;
//    }

// }


//int dimarch::db::get_dbid(std::string srvname)
int dimarch::oracle_backend::get_dbid(const char* srvname)
{

   int dbid = -1;



   std::map < std::string , int >::iterator dbidi = dbid_map.find(srvname);
   
   if (dbidi != dbid_map.end()) {
      return dbidi->second;
   }
   


   try {


      Statement* stmt = oraconn->createStatement();

      // query DB if service already exists

      stmt->setSQL("SELECT dbid FROM dimarch_services WHERE name=:1");
      stmt->setString(1,srvname);

      ResultSet *rs = stmt->executeQuery();
      
      if(rs->next()) {
         dbid =  rs->getInt(1);
      }

      stmt->closeResultSet (rs);
      oraconn->terminateStatement(stmt);

   }

   catch (SQLException &sqlExcp) {
      cerr << "Caught an exception: " << sqlExcp.what() << endl;
      
      throw;
   }

         

   if (dbid != -1) {
      // we found a valid DBID -> return it

      dbid_map[srvname] = dbid;
      return dbid;
   }


   try {

      
      Statement* stmt = oraconn->createStatement();

      
      // insert service name into dimarch DB
      stmt->setSQL("INSERT INTO dimarch_services (name) VALUES (:1)");
      stmt->setString(1,srvname);
      stmt->executeUpdate();
      oraconn->commit();
      
      // query newly created DBID
      stmt->setSQL("SELECT dbid FROM dimarch_services WHERE name=:1");
      stmt->setString(1,srvname);
      ResultSet *rs = stmt->executeQuery();
   
      
      if(rs->next()) {
         dbid =  rs->getInt(1);
      }

      stmt->closeResultSet (rs);
      oraconn->terminateStatement(stmt);
	 
   }

   catch (SQLException &sqlExcp) {
      cout << "Caught an exception: " << sqlExcp.what() << endl;
      
      return -1;
   }



   if (dbid != -1) {
      // we found a valid DBID -> return it

      dbid_map[srvname] = dbid;
      return dbid;
   }


   cerr << "no DBID found for service " << srvname << endl;
   return -1;
}




// boost::posix_time::ptime 
// dimarch::oracle2boost_time(const oracle::occi::Timestamp ts)
// {
    
//     int year, tzhour, tzmin;
//     unsigned int month, day, hour, min, sec, fs;

//     // extract information from oracle timestamp
//     ts.getDate           ( year,month, day );
//     ts.getTime           ( hour, min, sec, fs );
//     ts.getTimeZoneOffset ( tzhour, tzmin );

//     // oracle gives fractional seconds in nano-seconds, boost expects them in 
//     // micro-seconds, so we need to convert
//     fs /= 1000;

    
//     cout << setfill('0') 
//          << year << "-" << setw(2) << month << "-" << setw(2) << day << " "
//          << setw(2) << hour << ":" << setw(2) << min << ":" << setw(2) << sec 
//          << "." << fs
//          << "   " << setw(2) << tzhour << ":" << setw(2) << tzmin 
//          << setfill(' ') << endl;

//     // cout << ts.toText("YYYY-MM-DD HH24:MI:SS",0) << endl;



//     // construct and return boost ptime object
//     return boost::posix_time::ptime(boost::gregorian::date(year,month,day),
//                                     boost::posix_time::time_duration(hour+tzhour, 
//                                                                      min+tzmin,
//                                                                      sec,fs));
                          
// }

// oracle::occi::Date
// dimarch::boost2oracle_time(const boost::posix_time::ptime t,
//                            oracle::occi::Environment* env)
// {
   
//    boost::gregorian::date           bdate = t.date();
//    boost::posix_time::time_duration btime = t.time_of_day();

//    //boost::local_time::time_zone      bzone = t.zone();
//    //boost::posix_time::microsec_clock::local_time().zone();

//    cout << 42 << endl;

//    // oracle::occi::Timestamp oratime;
//    // oratime.setDate(bdate.year(), bdate.month(), bdate.day());
//    // oratime.setTime(btime.hours(), btime.minutes(), btime.seconds(), 0);
//    // oratime.setTimeZoneOffset(0,0); // needs testing...

//    oracle::occi::Date oratime( env, 
//                                bdate.year(), bdate.month(), bdate.day(), 
//                                btime.hours(), btime.minutes(), btime.seconds()); 

//    //cout << oratime.toText("YYYY-MM-DD",0) << endl;

//    return oratime;

// }




// Local Variables:
//   mode: c++
//     c-basic-offset: 4
//     indent-tabs-mode: nil
// End:
