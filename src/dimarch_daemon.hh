#ifndef DIMARCH_DAEMON_HH
#define DIMARCH_DAEMON_HH


#include "service.hh"
#include "dimarch.hh"

#include "dic.hxx"
#include <boost/regex.hpp>

#include <list>
#include <map>


class dimarch_daemon : public DimClient
{

public:

   dimarch_daemon();
   ~dimarch_daemon();

   //void subscribe(const char * name_pattern, float deadband);
   void subscribe(std::string name_pattern,
		  float nolink, float abs_deadband, float rel_deadband, int timeout);

   void infoHandler();

   void update_connections();

   void add_service(std::string name, std::string format,
		    float nolink, float abs_deadband, float rel_deadband,
		    int timeout);

   int find_dbid(std::string srvname);

   void show_subscriptions();

   unsigned int get_n_dim_updates() { return n_dim_updates; }
   unsigned int get_n_db_updates()  { return n_db_updates; }

protected:

   dimarch::dimarch *dimarchdb;

   void handle(dimarch::service &serv);

   struct subscription_t {
      std::string pattern;
      float nolink;
      float rel_deadband;
      float abs_deadband;
      int    timeout;
   };

   std::list<subscription_t> subscriptions;

   typedef std::map<DimInfo *,dimarch::service *> service_map_t;
   service_map_t service_map;

   unsigned int n_dim_updates;
   unsigned int n_db_updates;

};



#endif
