
dimarch  -  Archive DIM services in an Oracle database

(c) 2008-2012 Tom Dietel <tom@dietel.net>


Introduction
============

The dimarch utilities allow to archive DIM data in an oracle database
and retrieve it again. So far, the archival daemon (dimarchd), a
simple example retrieval program (dimarch-retrieve), and a ROOT class
to retrieve archived data into various ROOT data structures exist.


Archival Daemon: dimarchd
=========================

dimarchd subscribes to the DIM services specified in the configuration
file, checks all changes on these services against the last archived
value and writes the new data to the database if it differs from the
last archived value by more than a specified deadband.

If new DIM servers (dis-)appear in the network, dimarchd tries to
(un-)subscribe the newly available services.

Configuration File
------------------

The configuration file for services to be archived resides in
@sysconfdir@/trd/dimarch.conf. @sysconfdir@ defaults to
/usr/local/etc, but can be changed with the --sysconfdir=DIR switch of
the configure script.

Each line contains a key word and an argument. The following key words
are defined:

service: a service to subscribe to. Shell-style wildcards (as
	implemented in DimBrowser) can be used to specify multiple
	services at once. All following lines until the next service 
	line will refer to this group of services.

absolute_deadband: the service is only written to the DB if the
	absolute change since the last write exceeds the specified 
	dead band.

relative_deadband: the service is only written to the DB if the
	relative change since the last write exceeds the specified
	dead band.

	If both an absolute_deadband and a relative_deadband are
	given, changes are only written if the value is outside both
	deadbands.

nolink: value to be saved when the service is not available.

timeout: time after which a write to the DB is forced.



The configuration file is only read on startup. 


Example client: dimarch-retrieve
================================

This is just an example retrieval program. dimarch-retrieve retrieves
all data stored in the DB and writes it to standard output. the
program can be used as a starting point for more advanced clients.


ROOT Interface
==============

A class TDimArch provides access to the archive from within root. It
can be loaded into the ROOT C++ interpreter cint for interactive
use. The main functionality is to list the names of services stored in
the DB and to retrieve the data into a TNtuple or TTree. The class can
be instantiated without arguments:

	TDimArch da;

The retrieval into a TNtuple gets one tuple per stored value. The
columns of the TNtuple are the DB ID of the service, the value, the
time when the value was inserted into the DB (as given by the Oracle
DB server) in seconds since epoch and the time when the value was
published by the DIM server, as given by the DIM server. The preferred
method is:

	nt = da.GetNtupleD(colspec, start_time, end_time);

colspec is a an Oracle-style pattern to select the services to be
returned. A single service name is the most common example. Start and
end date can be given in any form understood by the date(1) command.
The times are interpreted in the system time zone.

The TTree interface mangles several values into one tuple, such that
each value has its own branch in the generated tree. In addition, the
branch "ti" holds the time in seconds since epoch (Jan 1, 1970
00:00:00 UTC). The usage is:

	tree = da.GetMultValues(colselect, start_time, end_time);

colselect is a comma separated list of services to be retrieved. Each
service name can be followed by a ">" and a string; in this case the
branch in the tree will be named like the text after the ">". For
example: "A_LONG_SERVICE_NAME>x,ANOTHER_NASTY_SERVICE_NAME>y,SRV"
would retrieve the archived data of the services A_LONG_SERVICE_NAME,
ANOTHER_NASTY_SERVICE_NAME and SRV, rename A_LONG_SERVICE_NAME to x,
ANOTHER_NASTY_SERVICE_NAME to y and store all three values as branches
in a tree.

Times can be specified in the same way as for the TNtuple interface.




Bugs
====

NONE - that I know of.

But there is still a lot of functionality missing:

 - dimarchd does not fork to background - seems this is related to OCCI
 - the DB connection data is still hardcoded, it should be read from a 
   config file




